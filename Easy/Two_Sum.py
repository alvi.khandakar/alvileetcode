"""
1/28/2023
https://leetcode.com/problems/two-sum/

Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.

Input: nums = [2,11,7,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 2].


Constraints:

2 <= nums.length <= 104
-109 <= nums[i] <= 109
-109 <= target <= 109
Only one valid answer exists.
"""

class Solution:
    def helper():
        pass

    def twoSum(self, nums: List[int], target: int) -> List[int]:
        solution = []
        dictionary = {}
        for i, j in enumerate(nums):
            needed_val = target - j
            if needed_val in dictionary:
                return [dictionary[needed_val], i]
            else:
                dictionary[j] = i


# 62 ms runtime
