"""
2/5/2023
# 20
Easy
https://leetcode.com/problems/valid-parentheses/
20 min

Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

An input string is valid if:

Open brackets must be closed by the same type of brackets.
Open brackets must be closed in the correct order.
Every close bracket has a corresponding open bracket of the same type.

Example 1:
Input: s = "()"
Output: true

Example 2:
Input: s = "()[]{}"
Output: true

Example 3:
Input: s = "(]"
Output: false
"""

class Solution:
    def isValid(self, s: str) -> bool:
        result = True
        lst = ([*s])
        for i in range(0, len(lst), 2):
            if lst[i] == "(":
                if (lst[i+1]) != ")":
                    result = False
            if lst[i] == "[":
                if lst[i+1] != "]":
                    result = False
            if lst[i] == "{":
                if lst[i+1] != "}":
                    result = False
        return result

# FIRST VERSION DIDN'T WORK BECAUSE THE REAL TESTCASE LOOKS LIKE "{[]}"

"""
added testcases; to more accurately test solution parameters:

Case 4: s = "{[]}"
Output: True

Case 5: s = "{[]}()"
Output: True

Case 6: s = "{[]}(}"
Output: False
"""

"""
Testing a for loop on a string
"""

def for_tester(string):
  for char in string:
    print(char)
  return "done"

for_tester("{[()]}") # -->
"""
{
[
(
)
]
}
"""
"""
Testing dictionary: if a string is in dictionary
"""
def if_in_dict(string):
  dictionary = {
    '(' : ')',
    '{' : '}',
    '[' : ']',
    }
  for char in string:
    if char in dictionary:
      print("In here", char)
    else:
      print("not")
  return "complete"

if_in_dict("({[]})") # --> All left brackets in, no right brackets, ONLY CHECKS IF IN KEYS

"""
Testing .pop() of a list of single char strings
"""
def pop_tester():
  list = [ '(', '{', '[', ]
  print(list.pop()) # --> [
    # the .pop() method itself will STORE the removed character and mutate the original list
    # This is important because that popped character is still accessible
    # In our example we use this to our advantage

pop_tester()



class Solution:
  def isValid(self, s: str) -> bool:
    """
    :type s: str
    :rtype: bool
    """
    dictionary = {
      '(' : ')',
      '{' : '}',
      '[' : ']',
      }
      # THIS CAN BE SPED UP BY COMBINGING LINES 113 - 117
    checker = []
    for char in s:
      if char in dictionary: # 1
        checker.append(char)
      # elif is only executed if previous statement is FALSE
      elif len(checker) == 0 or dictionary[checker.pop()] != char: # 2
        return False
    return len(checker) == 0 # 3

# 1. if it's the left bracket then we append it to the stack
# 2. else if it's the right bracket and the stack is empty(meaning no matching left bracket), 
#       or the left bracket doesn't match
# 3. finally check if the stack still contains unmatched left bracket

