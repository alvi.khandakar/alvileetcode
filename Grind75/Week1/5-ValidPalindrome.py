"""
2/7/2023
# 125
Easy
https://leetcode.com/problems/valid-palindrome/description/
15 min

A phrase is a palindrome if, after converting all uppercase letters into
lowercase letters and removing all non-alphanumeric characters, it reads 
the same forward and backward. Alphanumeric characters include letters 
and numbers.

Given a string s, return true if it is a palindrome, or false otherwise.


Example 1:
Input: s = "A man, a plan, a canal: Panama"
Output: true
Explanation: "amanaplanacanalpanama" is a palindrome.

Example 2:
Input: s = "race a car"
Output: false
Explanation: "raceacar" is not a palindrome.

Example 3:
Input: s = " "
Output: true
Explanation: s is an empty string "" after removing non-alphanumeric
characters.
Since an empty string reads the same forward and backward, it is a
palindrome.
"""

class Solution:
    def isPalindrome(self, s: str) -> bool:
        string = ''.join(char for char in s if char.isalnum())
        string = string.lower()
        reverse = string[::-1]
        if reverse == string:
            return True
        return False

# Initial attempt 44 ms runtime, 83.88%

"""

Can be improved by linstead of using a join function just using a for 
loop, just concate it to a new string. Before actually concating it you 
check if alpha numeric, and changing everything to lower as we add it to 
new string. Comparing newStr directly to the inverse slice of itself

"""


class Solution:
    def isPalindrome(self, s: str) -> bool:
        newStr = ""
        for c in s:
            if c.isalnum():
                newStr += c.lower()
        return newStr == newStr[::-1]

# Can even be further improved by pushing line 57-59 together: 
# List Comprehension, study it.

class Solution:
    def isPalindrome(self, s: str) -> bool:
        s = [i for i in s.lower() if i.isalnum()]
        return s == s[::-1]

"""
Returns a 38 ms runtime with 95.18%
"""

