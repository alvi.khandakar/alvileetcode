"""
2/7/2023
# 704
Easy
https://leetcode.com/problems/binary-search/
15 min

Given an array of integers nums which is sorted in ascending order, and an integer target, write a function to search target in nums. If target exists, then return its index. Otherwise, return -1.

You must write an algorithm with O(log n) runtime complexity.

Example 1:
Input: nums = [-1,0,3,5,9,12], target = 9
Output: 4
Explanation: 9 exists in nums and its index is 4

Example 2:
Input: nums = [-1,0,3,5,9,12], target = 2
Output: -1
Explanation: 2 does not exist in nums so return -1
"""

"""
IMMEDIATE THOUGHT: Pop each element and check one at a time
PROBLEM: This is O(n^2)
UNKNOWN: How to do a binary search lol
"""
class Solution:
    def search(self, nums: List[int], target: int) -> int:
        count = 0
        maxCount = len(nums)
        while count < maxCount:
            if nums.pop(0) == target:
                return count
            else:
                count += 1
        return -1

# Runtime 338 ms; 20.35%
# Memory: 15.6 mb; 18.2%
# BAD Time complex: O(n^2)
# Time: 8:54 / 15:00


"""
LEARN BINARY SEARCH JFC
This is ALREADY Sorted, so you CAN use a binary search
--- NOTE: if sorted, you can do a sort

HOW Binary Search is done:
+ set your left and right points which will be the first and last index
+ set while statement to stop when you're indexing the same value
+ Create a midpoint  of the section you're wanting to check, first 
iteration it is the whole set
+ Check for if the target == midpoint
+ Check if target > or < midpoint -- That is the direction we check
+ Reset your left and right according to what side we checking
+ While reiterates this till found -- Will be found by target == midpoint
"""
class Solution:
    def search(self, nums: List[int], target: int) -> int:
        left, right = 0, len(nums)-1
        while left <= right:
            mid = (left+right)//2

            if nums[mid] == target:
                return mid
            elif nums[mid] < target:
                left = mid + 1
            else:
                right = mid - 1
        
        return -1

# Runtime: 229 ms; 99.6%
# Memory 15.5 mb; 63.81%
# ACTUAL O(n log n)