"""
2/7/2023
# 226
Easy
https://leetcode.com/problems/invert-binary-tree/
15 min

Given the root of a binary tree, invert the tree, and return its root.

Input: root = [4,2,7,1,3,6,9]
Output: [4,7,2,9,6,3,1]

"""

# Definition for a binary tree node.

"""
Original thought process:

Build a new Tree
Call on the root head
For node in root
make self.left = new.right
make self.right = new.left
"""

#REVISED
"""
https://www.youtube.com/watch?v=OnSn2XEQ4MY
+ Use a depth first search and recursive instead
+ This also reduces the memory footprint as it transform the tree
internally rather than creating a new tree
+ Only stores using one new variable: tmp
+ Uses a recursive to work through the rest of the tree
+ Call the recursion for each left and right node

Check for edge-case of root = None
"""

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def invertTree(self, root: Optional[TreeNode]) -> Optional[TreeNode]:
        if not root:
            return None
        tmp = root.left
        root.left = root.right
        root.right = tmp
        self.invertTree(root.left)
        self.invertTree(root.right)
        return root

# Runtime 32 ms, 74.65%
# Memory 13.8 mb, 49.52%

"""
Can cut down time by operating only IF root bool is True
Can flip roots in place together using similar to i, j = j, i  logic
"""

class Solution:
    def invertTree(self, root: Optional[TreeNode]) -> Optional[TreeNode]:
        if root:
            root.left, root.right = self.invertTree(root.right), self.invertTree(root.left)
        return root