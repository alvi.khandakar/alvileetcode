"""
2/9/2023
# 110
Easy
https://leetcode.com/problems/balanced-binary-tree/

Given a binary tree, determine if it is Height Balanced --

Height-Balanced:
A height-balanced binary tree is a binary tree in which the depth of the 
two subtrees of every node never differs by more than one.

Example 1:
Input: root = [3,9,20,null,null,15,7]
Output: true

Example 2:
Input: root = [1,2,2,3,3,null,null,4,4]
Output: false

Example 3:
Input: root = []
Output: true
"""

"""
IMMEDIATE THOUGHT: I can do this. 
PROBLEM: 
UNKNOWN: 
"""

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right    

class Solution:
    def isBalanced(self, root: Optional[TreeNode]) -> bool:
        self.Bal = True
        self.dfs(root)
        return self.Bal

    def dfs(self, node):
        if not node: 
            return 0
        left, right = self.dfs(node.left), self.dfs(node.right)
        if abs(left - right) > 1:
            self.Bal = False
        return max(left, right) + 1