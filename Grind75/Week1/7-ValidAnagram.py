"""
2/7/2023
# 242
Easy
https://leetcode.com/problems/valid-anagram/description/
15 min

Given two strings s and t, return true if t is an anagram of s, and 
false otherwise.

An Anagram is a word or phrase formed by rearranging the letters of a
different word or phrase, typically using all the original letters
exactly once.

Example 1:
Input: s = "anagram", t = "nagaram"
Output: true

Example 2:
Input: s = "rat", t = "car"
Output: false
"""

"""
Immediate thought: Use dictionaries and for loops.
Problem: Multiple for loops may not be the best, time complexity-wise
"""
class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        d1 = {}
        d2 = {}
        for char in s:
            if char in d1:
                d1[char] += 1
            else:
                d1[char] = 1
        for char in t:
            if char in d2:
                d2[char] += 1
            else:
                d2[char] = 1
        return d1 == d2

# Runtime 46 ms, 83.80% -- Surprisngly not bad
# Memory 14.4 mb, 62.2%

"""
EASIEST TIME SPEEDER: Check string lengths lol

Instead of using a for loop on the strings, use for loop on a-z letters. This makes that time complexity constant O(26) 
Count the occurence of the letter in the strings, and as soon as either of strings don't have equal counts for a single letter, return false

else True
"""

class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        if len(s) != len(t):
            return False

        letters = 'abcdefghijklmnopqrstuvwxyz'
        for letter in letters:
            if s.count(letter) != t.count(letter):
                return False
        
        return True

# Runtime 31 ms; 99.29%
# Memory: 14.3 mb; 96.57%

