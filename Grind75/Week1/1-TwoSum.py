"""
2/5/2023
# 1
Easy
https://leetcode.com/problems/two-sum/
15 min

Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.

Input: nums = [2,11,7,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 2].


Constraints:

2 <= nums.length <= 104
-109 <= nums[i] <= 109
-109 <= target <= 109
Only one valid answer exists.
"""


class Solution:
  def twoSum(self, nums: List[int], target: int) -> List[int]:
    results = []
    dictionary = {}
    for index, value in enumerate(nums):
      needed_number = target - value
      if needed_number in dictionary:
        results.append(dictionary[needed_number]) # since referring by needed, value should be the key
        results.append(index)
        return results
      dictionary[value] = index
    
# runtime 64 MS
# Could be improved by combining 31, 32

"""
Best answer below:
"""

class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        # Go through list once, determine target - num, save that number in a dict with index
        diffs = {}
        for i in range(0, len(nums)):
            if nums[i] in diffs:
                return [i, diffs[nums[i]]]
            d = target - nums[i]
            diffs[d] = i