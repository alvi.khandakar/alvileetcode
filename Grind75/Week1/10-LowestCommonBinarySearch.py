"""
2/7/2023
# 235
Medium
20 min
https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-search-tree/

Given a binary search tree (BST), find the lowest common 
ancestor (LCA) node of two given nodes in the BST.

According to the definition of LCA on Wikipedia: “The lowest 
common ancestor is defined between two nodes p and q as the 
lowest node in T that has both p and q as descendants 
(where we allow a node to be a descendant of itself).”
- https://en.wikipedia.org/wiki/Lowest_common_ancestor

Example 1:
Input: root = [6,2,8,0,4,7,9,null,null,3,5], p = 2, q = 8
Output: 6
Explanation: The LCA of nodes 2 and 8 is 6.

Example 2:
Input: root = [6,2,8,0,4,7,9,null,null,3,5], p = 2, q = 4
Output: 2
Explanation: The LCA of nodes 2 and 4 is 2, since a node can 
be a descendant of itself according to the LCA definition.

Example 3:
Input: root = [2,1], p = 2, q = 1
Output: 2
"""

"""
IMMEDIATE THOUGHT: I'll have to use a recursive function or a while statement for this..? How would I minimize time complexity?
PROBLEM: 
UNKNOWN: Don't know how to search or traverse a binary tree. Found some information o n how to search binary tree, but this doesn't count for inclusive if p or q are the parent of one or the other.
https://www.geeksforgeeks.org/lowest-common-ancestor-in-a-binary-search-tree/
"""

# Original answer only gives LCA that is not inclusive of p or q

class Solution:
    def lowestCommonAncestor(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
        def lca(root, p, q):
            if(root.val > p.val and root.val > q.val):
                if root.val == p.val or root.val == q.val:
                    return root
                return lca(root.left, p, q)
            if(root.val < p.val and root.val > q.val):
                if root.val == p.val or root.val == q.val:
                    return root
                return lca(root.right, p, q)
            return root
        return lca(root, p, q)

# Runtime 56 ms, DQ
# Memory: Uknown
# Time: DQ
# Checking Solution

class Solution:
    def lowestCommonAncestor(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
        lower = min(p.val, q.val)
        greater = max(p.val, q.val)
        while root:
            if root.val > greater: # going left
                root = root.left
            elif root.val < lower: # going right
                root = root.right
            else:
                return root
        return

# Runtime 78 ms; 82.13%
# Memory: 18.8 mb; 61.22%

"""
Can be further improved by getting rid of the max, min logic lines and instead include them in the if logic statement
"""
class Solution:
    def lowestCommonAncestor(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
        current = root

        while current:
            if current.val > p.val and current.val > q.val:
                current = current.left
            elif current.val < p.val and current.val < q.val:
                current = current.right
            else:
                return current
        return None
