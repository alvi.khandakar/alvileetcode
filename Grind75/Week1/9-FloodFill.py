"""
2/7/2023
# 733
Easy
20 Min
https://leetcode.com/problems/flood-fill/

An image is represented by an m x n integer grid image where 
image[i][j] represents the pixel value of the image.

You are also given three integers sr, sc, and color. You should 
perform a flood fill on the image starting from the 
pixel image[sr][sc].

To perform a flood fill, consider the starting pixel, plus any 
pixels connected 4-directionally to the starting pixel of the 
same color as the starting pixel, plus any pixels connected 4-directionally to those pixels (also with the same color), 
and so on. Replace the color of all of the aforementioned 
pixels with color.

Return the modified image after performing the flood fill.

Example 1:
Input: image = [[1,1,1],[1,1,0],[1,0,1]], sr = 1, sc = 1, color = 2
Output: [[2,2,2],[2,2,0],[2,0,1]]

image = [
  [1,1,1],
  [1,1,0],
  [1,0,1]
  ]

final = [
  [2,2,2],
  [2,2,0],
  [2,0,1]
]

Explanation: From the center of the image with position (sr, sc) = (1, 1) (i.e., the 
red pixel), all pixels connected by a path of the same color as the starting pixel 
(i.e., the blue pixels) are colored with the new color.
NOTE: the bottom corner is not colored 2, because it is not 4-directionally 
connected to the starting pixel.


Example 2:
Input: image = [[0,0,0],[0,0,0]], sr = 0, sc = 0, color = 0
Output: [[0,0,0],[0,0,0]]
Explanation: The starting pixel is already colored 0, so no changes are made to the image.
"""

"""
IMMEDIATE THOUGHT: Fuck. Okay I want to check (0, 1), (1,0), (0, -1), (-1, 0) from start point and do that continually. How do I set this for loop up???
PROBLEM: I don't know how to check this, but I'm pretty sure I use a for loop and check if they're equal to start_val
UNKNOWN: How to traverse a grid in 4 directions --> Depth First Search
https://www.youtube.com/watch?v=iaBEKo5sM7w

Ended up having to refer to answers, but was thinking of using a helper function recursively so was on to the right idea. Just didn't know how to write this helper function
+ The helper function is to traverse, but the DSA type is a DEPTH FIRST SEARCH, DFS

"""

class Solution:
    def floodFill(self, image: List[List[int]], sr: int, sc: int, color: int) -> List[List[int]]:
        start_val = image[sr][sc] # 1
        length = len(image)
        width = len(image[0])


        def traverse(row, col): # Create a helper function, we want to do this recursively
            if (not 
            (0 <= row < length and 0 <= col < width) # if the [sr][sc] outside of grid, continues the statement, which breaks this iteration, goes to next combo
            ) or image[row][col] != start_val: # Used in a for loop to check to ensure the else is only operating on values == start value, if checked num != start value, then it will break this iteration and go to next combo
                return # Pulls us out of this iteration if we have a bad grid index 
            image[row][col] = color # the actual change as an else statement, only does on grid points that DON'T fall in the above exception list
            [traverse(row + x, col + y) for(x, y) in ((0, 1), (1,0), (0, -1), (-1, 0))] # List comprehension, it will do this recursively for every iteration of 
            """
            https://www.w3schools.com/python/python_lists_comprehension.asp
            for x, y in ((0, 1), (1,0), (0, -1), (-1, 0)):
              traverse(row + x, col + y)
            """
        

        if start_val != color: # This is more clearly shown in ex 2, think of paint.exe's fill function
            traverse(sr, sc) # Uses the traverse function 
        return image # the image grid has changed, we are now just returning this changed grid

# Runtime 78 ms; 74.1%
# Memory 14.1 mb; 34.81%
# Time: 16:54 / 20
# DISQUALIFIED. Used answers

"""
This can be improved on by using an affirmative recursion instead 
of a negative recursion like we did above. Meaning that the if 
statements check IF the conditions for a change are met THEN
it will proceed and do a change

NOT the other way around

Also splits up the list comprehension of the function to doing 
each direction one at a time
"""
class Solution:
    def floodFill(self, image: List[List[int]], sr: int, sc: int, color: int) -> List[List[int]]:
        start_val = image[sr][sc]
        length, width = len(image), len(image[0])

        def dfs(row, col):
          if 0 <= row < length and 0 <= col < width:
            if image[row][col] == start_val and image[row][col] != color:
              image[row][col] = color

              dfs(row + 1, col)
              dfs(row, col + 1)
              dfs(row -1, col)
              dfs(row, col - 1)

        dfs(sr, sc)
        return image


# Runtime 62 ms; 99.1%
# Memory 14.2 mb; 34.61%
