"""
2/5/2023
# 21
Easy
https://leetcode.com/problems/merge-two-sorted-lists/
20 min

You are given the heads of two sorted linked lists list1 and list2.

Merge the two lists in a one sorted list. The list should be made by splicing together the nodes of the first two lists.

Return the head of the merged linked list.


Example 1:
Input: list1 = [1,2,4], list2 = [1,3,4]
Output: [1,1,2,3,4,4]

Example 2:
Input: list1 = [], list2 = []
Output: []

Example 3:
Input: list1 = [], list2 = [0]
Output: [0]
"""

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def mergeTwoLists(self, list1: Optional[ListNode], list2: Optional[ListNode]) -> Optional[ListNode]:
        filler_head = ListNode(0)
        current = filler_head
        while list1 != None or list2 != None:
            list1_val = list1.val if list1 else 101
            list2_val = list2.val if list2 else 101
            if list1_val <= list2_val:
                new_node = ListNode(list1_val)
                list1 = list1.next if list1 else None
            else:
                new_node = ListNode(list2_val)
                list2 = list2.next if list2 else None
            current.next = new_node
            current = new_node
        return filler_head.next

# 27 MS runtime, top 0.86% of submissions

