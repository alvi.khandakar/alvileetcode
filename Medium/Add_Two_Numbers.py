
def noNodeAddNumbers(l1, l2):
        x = ''
        y = ''
        for i1 in reversed(l1):
            stringed_num = str(i1)
            x += stringed_num
        for i2 in reversed(l2):
            stringed_num = str(i2)
            y += stringed_num
        x = int(x)
        y = int(y)
        added = x + y
        added = str(added)
        txt = added[::-1]
        result_number = int(txt)
        result_list = list(map(int, str(result_number)))
        return result_list

# print (noNodeAddNumbers([2,4,3], [5,6,4]))


# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def addTwoNumbers(self, l1, l2):
        filler_head = ListNode(0)
        current = filler_head
        carry = 0
        while l1 != None or l2 != None or carry != 0:
            l1_val = l1.val if l1 else 0
            l2_val = l2.val if l2 else 0
            digit_sum = l1_val + l2_val + carry
            carry = digit_sum // 10
            new_Node = ListNode(digit_sum % 10)
            current.next = new_Node
            current = new_Node
            l1 = l1.next if l1 else None
            l2 = l2.next if l2 else None
        return filler_head.next

    
def print_list(head):
  current = head 
  while current is not None:
    print(current.val)
    current = current.next
    
a = ListNode(2)
b = ListNode(4)
c = ListNode(3)

a.next = b
b.next = c

l1 = a


d = ListNode(5)
e = ListNode(6)
f = ListNode(4)

d.next = e
e.next = f

l2 = d

print_list(Solution().addTwoNumbers(l1, l2))